from  tkinter import *
import math


# ---------------------------- CONSTANTS ------------------------------- #
PINK = "#e2979c"
RED = "#e7305b"
GREEN = "#9bdeac"
YELLOW = "#f7f5dd"
FONT_NAME = "Courier"
WORK_MIN = 25
SHORT_BREAK_MIN = 5
LONG_BREAK_MIN = 20
reps = 0
timer = None

# ---------------------------- TIMER RESET ------------------------------- # 
def timer_reset():
    window.after_cancel(timer)
    timer_label.config(text = "Timer",font =(FONT_NAME,35,"bold"),fg=GREEN,bg=YELLOW)
    canvas.itemconfig(timer_text,text="00:00")
    check_mark.config(text = "")
# ---------------------------- TIMER MECHANISM ------------------------------- # 

def start_timer():
    global reps
    reps += 1
    
    
    work_sec = WORK_MIN *60
    short_break_sec = SHORT_BREAK_MIN *60
    long_break_sec = LONG_BREAK_MIN *60
    
    
    if reps % 8 == 0:
        timer_label.config(text = "Break",font=(FONT_NAME,35,"bold"),fg=RED)
        count_down(long_break_sec)
    elif reps % 2 == 0:
        timer_label.config(text = "Break",font=(FONT_NAME,35,"bold"),fg=PINK)
        count_down(short_break_sec)
    else:
        timer_label.config(text = "Timer",font =(FONT_NAME,35,"bold"),fg=GREEN,)
        count_down(work_sec)
    

# ---------------------------- COUNTDOWN MECHANISM ------------------------------- # 

def count_down(count):
    global timer
    count_min = math.floor(count / 60)
    count_sec = int(count % 60)

    canvas.itemconfig(timer_text,text=f"{count_min}:{count_sec:02d}")
    if count > 0:
         timer = window.after(1000,count_down,count -1)
    else:
        start_timer()
        mark = ""
        work_sessions = math.floor(reps/2)
        for _ in range(work_sessions):
            mark += "✔"
        check_mark.config(text = mark)


# ---------------------------- UI SETUP ------------------------------- #
window = Tk()
window.title("Pomodoro")
window.config(padx=100,pady=50,bg=YELLOW)


#image of the timer
canvas = Canvas(width = 215, height = 224 ,bg=YELLOW,highlightthickness=0)
IMG = PhotoImage(file="tomato.png")
canvas.create_image(103, 112,image = IMG)
timer_text = canvas.create_text(103,130,text="00:00",fill="white",font=(FONT_NAME,35,"bold"))
canvas.grid(column=1,row=1)



#Timer label
timer_label = Label(text = "Timer",font =(FONT_NAME,35,"bold"),fg=GREEN,bg=YELLOW)
timer_label.grid(column =1,row =0)
#Start button
start_button = Button(text = "Start",highlightthickness=0,command=start_timer)
start_button.grid(column = 0,row =2 )
#Check mark
check_mark = Label(font = (FONT_NAME,18,"bold"),fg=GREEN,bg=YELLOW)
check_mark.grid(column = 1,row = 3)
#stop button
reset_button = Button(text="Reset",highlightthickness=0,command=timer_reset)
reset_button.grid(column = 2,row = 2)




window.mainloop()